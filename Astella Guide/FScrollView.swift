//
//  FScrollView.swift
//  FruitfulLibraries
//
//  Created by Zee on 07/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit


public class FScrollView : UIScrollView {
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    public override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        self.nextResponder()?.touchesEnded(touches, withEvent: event)
    }
    
    public override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        self.nextResponder()?.touchesBegan(touches, withEvent: event)
    }
    
    
}