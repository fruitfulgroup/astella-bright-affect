//
//  JSONUrlRequest.swift
//  FruitfulLibraries
//
//  Created by Gaurav Ravindra on 01/05/2015.
//  Copyright (c) 2015 Fruitful Business Services. All rights reserved.
//

import Foundation
import UIKit


public class JSONUrlRequest {
    
    var request:NSMutableURLRequest
    var body:NSMutableData
    public var ParsedObjects:NSMutableArray
    
    private var alert_Refreshing:UIAlertController!
    public var OnRequestBegin:(() -> ())?
    public var OnRequestEnd:(() -> ())?
    public var OnRequestSuccess:((result:String) -> ())?
    public var OnRequestFail:((result:String) -> ())?
    
    public var CustomRefreshView:(UIView)?
    
    let API_BOUNDARY:(String) = "--StopStiffingOurPostDataYouBanana"
    
    //private let quene = dispatch_queue_create("serial-worker", DISPATCH_QUEUE_CONCURRENT)
    
    
    init() {
        self.request = NSMutableURLRequest()
        self.body = NSMutableData()
        self.request.HTTPMethod = "POST"
        self.ParsedObjects = NSMutableArray()
        request.addValue("multipart/form-data; boundary=\(API_BOUNDARY)", forHTTPHeaderField: "Content-Type")
        
        
        self.alert_Refreshing = UIAlertController(title: "Please Wait...", message: nil, preferredStyle: .Alert)
        /*
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(50, 10, 37, 37)) as UIActivityIndicatorView
        loadingIndicator.center = self.alert_Refreshing.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        loadingIndicator.startAnimating();
        
        self.alert_Refreshing.setValue(loadingIndicator, forKey: "accessoryView")
*/
    }
    
    func SetURL(UrlString UrlString:String) {
        if let link_encoded = UrlString.stringByReplacingOccurrencesOfString("~/", withString: Fruitful.API_URL).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            
            self.request.URL = NSURL(string: link_encoded)
        }
    }
    
    func AddParameter(Name n:String, Value v:String){
        self.body.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Disposition: form-data; name=\"\(n)\"\r\n\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("\(v)").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
    }
    
    func AddFile(Name n:String, Data d:NSData){
        self.body.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Disposition: form-data; name=\"\(n)\"; filename=\"%@\"\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(String("Content-Type: application/octet-stream\r\n\r\n\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        self.body.appendData(d)
        self.body.appendData(String("\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
    }
    
    func GetRequest() -> NSMutableURLRequest {
        
        let newBody:(NSMutableData) = NSMutableData(data: self.body)
        newBody.appendData(String("--\(API_BOUNDARY)\r\n").dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)!)
        
        let newRequest:(NSMutableURLRequest) = NSMutableURLRequest()
        newRequest.HTTPMethod = "POST"
        newRequest.URL = self.request.URL
        newRequest.addValue("multipart/form-data; boundary=\(API_BOUNDARY)", forHTTPHeaderField: "Content-Type")
        
        let dataString:String = NSString(data: newBody, encoding: NSUTF8StringEncoding) as! String
        
        #if DEBUG
            print("Request URL: \(newRequest.URL)")
            print("Request DATA: \(dataString)")
        #endif
        
        newRequest.HTTPBody = newBody
        return newRequest
    }
    
    
    func GetSingleObjectFromUrlRequest(ShowAlert ShowAlert:Bool){
        
        if(ShowAlert){ self.ShowRefreshAlert() }
        if let v = self.CustomRefreshView { v.hidden = false }
        if let _ = self.OnRequestBegin { self.OnRequestBegin!() }
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(req, completionHandler: {(data, response, error) in
            
            if let data:NSData = data {
                
                #if DEBUG
                    if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                        print("\(returnString)")
                    }
                #endif
                
                if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary{
                    
                    if let text:(String) = json.objectForKey("error") as? String {
                        //return (false, text)
                        if let _ = self.OnRequestFail { self.OnRequestFail!(result: text) }
                    }
                    
                    //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                    self.ParsedObjects.removeAllObjects()
                    self.ParsedObjects.addObject(json)
                    if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: "JSON returned") }
                    //return (true, "JSON returned")
                }
                else{
                    if let _ = self.OnRequestFail { self.OnRequestFail!(result: "No JSON was returned") }
                    //return (false, "No JSON was returned")
                }
            }
            else{
                if let _ = self.OnRequestFail { self.OnRequestFail!(result: "Unable to connect to database. Please check you have internet connection.")
                //return (false, "Unable to connect to database. Please check you have internet connection.")
            }
                
            dispatch_async(dispatch_get_main_queue(), {
                if(ShowAlert){ self.DismissRefreshAlert() }
                if let v = self.CustomRefreshView { v.hidden = false }
                if let _ = self.OnRequestEnd { self.OnRequestEnd!() }
            })
        }
            
        });
        task.resume()
        
    }
    
    
    func GetJsonFromUrlRequest(ShowAlert ShowAlert:Bool){
        if(ShowAlert){ self.ShowRefreshAlert() }
        if let v = self.CustomRefreshView { v.hidden = false }
        if let _ = self.OnRequestBegin { self.OnRequestBegin!() }
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(req, completionHandler: {(data, response, error) in
            
            if let data:NSData = data {
                
                #if DEBUG
                    if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                        print("\(returnString)")
                    }
                #endif
                
                if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary{
                    
                    if let text:(String) = json.objectForKey("error") as? String {
                        if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: text) }
                    }
                    else{
                            if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: "JSON returned") }
                            //return (true, "JSON returned")

                    }
                }
                
                if let allObjects:NSArray = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSArray {
                    //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                    self.ParsedObjects.removeAllObjects()
                    self.ParsedObjects.addObjectsFromArray(allObjects as [AnyObject])
                    if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: "JSON returned") }
                    //return (true, "JSON returned")

                }
                else{
                    if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: "JSON returned") }
                    //return (true, "JSON returned")

                }            }
            else{
                if let _ = self.OnRequestFail { self.OnRequestFail!(result: "Unable to connect to database. Please check you have internet connection.")
                    //return (false, "Unable to connect to database. Please check you have internet connection.")
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if(ShowAlert){ self.DismissRefreshAlert() }
                    if let v = self.CustomRefreshView { v.hidden = false }
                    if let _ = self.OnRequestEnd { self.OnRequestEnd!() }
                })
            }
            
        });
        task.resume()
    }
    
    
    func GetStringFromUrlRequest(ShowAlert ShowAlert:Bool){
        
        if(ShowAlert){ self.ShowRefreshAlert() }
        if let v = self.CustomRefreshView { v.hidden = false }
        if let _ = self.OnRequestBegin { self.OnRequestBegin!() }
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(req, completionHandler: {(data, response, error) in
            
            if let data:NSData = data {
                var returnString = ""
                #if DEBUG
                if let rtnStr:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                    print("\(rtnStr)")
                    returnString = rtnStr
                }
                #endif
                
                if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary{
                    
                    if let text:(String) = json.objectForKey("error") as? String {
                        //return (false, text)
                        if let _ = self.OnRequestFail { self.OnRequestFail!(result: text) }
                    }
                    else{
                        if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: returnString) }
                    }
                }
                else{
                    if let _ = self.OnRequestSuccess { self.OnRequestSuccess!(result: returnString) }
                }
            }
            else{
                if let _ = self.OnRequestFail { self.OnRequestFail!(result: "Unable to connect to database. Please check you have internet connection.")
                    //return (false, "Unable to connect to database. Please check you have internet connection.")
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if(ShowAlert){ self.DismissRefreshAlert() }
                    if let v = self.CustomRefreshView { v.hidden = false }
                    if let _ = self.OnRequestEnd { self.OnRequestEnd!() }
                })
            }
            
        });
        task.resume()
    }
    
    
    /*
    private func _getJson() -> (sucess:Bool, text:String) {
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        let session = NSURLSession.sharedSession()

        let task : NSURLSessionDataTask = session.dataTaskWithRequest(req, completionHandler: {(data, response, error) in
            
            if let data:NSData = data {
            
            #if DEBUG
                if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                    print("\(returnString)")
                }
            #endif
            
            if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary{
                
                if let text:(String) = json.objectForKey("error") as? String {
                    return (false, text)
                }
                
                //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                ParsedObjects.removeAllObjects()
                ParsedObjects.addObject(json)
                return (true, "JSON returned")
            }
            else{
                return (false, "No JSON was returned")
            }
            
        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
            
        });
        
    }
    
    private func _getJsonArray() -> (sucess:Bool, text:String) {
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        if let data:NSData = try? NSURLConnection.sendSynchronousRequest(req, returningResponse: nil) {
            
            #if DEBUG
                if let returnString:String = NSString(data: data, encoding: NSUTF8StringEncoding) as? String{
                    print("\(returnString)")
                }
            #endif
            
            if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary{
                
                if let text:(String) = json.objectForKey("error") as? String {
                    return (false, text)
                }
                else{
                    return (false, "No JSON was returned")
                }
            }
            
            if let allObjects:NSArray = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSArray {
                //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
                ParsedObjects.removeAllObjects()
                ParsedObjects.addObjectsFromArray(allObjects as [AnyObject])
                return (true, "JSON returned")
            }
            else{
                return (false, "No JSON was returned")
            }
            
        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
        
    }
    
    
    
    private func _getString() -> (sucess:Bool, text:String) {
        
        let req:NSMutableURLRequest = self.GetRequest()
        
        if let data:NSData = try? NSURLConnection.sendSynchronousRequest(req, returningResponse: nil){
            
            
            let returnString:NSString = NSString(data: data, encoding: NSUTF8StringEncoding)!
            
            #if DEBUG
                print("\(returnString)")
            #endif
            
            if let json:NSDictionary = (try? NSJSONSerialization.JSONObjectWithData(data, options: [])) as? NSDictionary {
                
                if let text:String = json.objectForKey("error") as? String{
                    return (false, text)
                }
            }
            
            /*
            var dic:NSDictionary = NSDictionary()
            dic.setValue(returnString, forKey: "Text")
            
            //Only Clear Parsed Objects List if the JSON was successful. Funtion wouldn't go past
            ParsedObjects.removeAllObjects()
            ParsedObjects.addObject(dic)
            */
            
            return (true, String(returnString))
            
        }
        else{
            return (false, "Unable to connect to database. Please check you have internet connection.")
        }
        
        
    }
*/
    
    
    private func ShowRefreshAlert() {
        if let topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
            if let presentedViewController = topController.presentedViewController {
                presentedViewController.presentViewController(self.alert_Refreshing, animated: true, completion: nil)
            }
            else{
                topController.presentViewController(alert_Refreshing, animated: true, completion: nil)
            }
        }
    }
    
    
    private func DismissRefreshAlert() {
        alert_Refreshing.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
}


