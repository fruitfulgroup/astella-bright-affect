//
//  LoginView.swift
//  Bright Affect
//
//  Created by Zee on 20/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit

class LoginView: UIViewController {
    
    @IBOutlet var tf_username: UITextField?
    @IBOutlet var tf_password: UITextField?
    
    private var alert_Refreshing:UIAlertController!

    override func viewDidLoad() {
        
        ///DELETE IF NEED TO RE-ADD LOGIN SCREEN
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("CoursesTableView") as? CoursesTableView {
            self.navigationController?.pushViewController(view, animated: false)
            return
        }
        
        tf_username?.text = FLoginControl.UserName()

        if FLoginControl.IsLoggedIn() {
            
            if let view = self.storyboard?.instantiateViewControllerWithIdentifier("MainMenu") as? MainMenuView {
                self.navigationController?.pushViewController(view, animated: false)
            }
        }
        
        self.alert_Refreshing = UIAlertController(title: FLocal.LocalizedString("LOGGING_IN", comment: ""), message: nil, preferredStyle: .Alert)
        
    }
    
    @IBAction func PickLanguage(sender: UIBarButtonItem) {
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        standardUserDefaults.removeObjectForKey("LangID")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func Login(sender: UIButton) {
        
        self.view.endEditing(true)
        self.presentViewController(alert_Refreshing, animated: true, completion: nil)
        FLoginControl.SetOnLoginSuccess(LoginSuccess)
        FLoginControl.SetOnLoginFail(LoginFail)
        FLoginControl.Login(User: tf_username!.text!, Password: tf_password!.text!)
        
    }
    
    
    func LoginSuccess() {
        self.performSegueWithIdentifier("LoginSuccess", sender: self)
    }
    
    func LoginFail(text: String) {
        
        var alert:UIAlertController!
        alert_Refreshing.dismissViewControllerAnimated(false, completion: nil)

        
        if text.lowercaseString.rangeOfString("database") != nil {
            alert = UIAlertController(title: nil, message: FLocal.LocalizedString("DATABASE_CONNECT_FAIL", comment: ""), preferredStyle: .Alert)
        }
        else{
           alert = UIAlertController(title: nil, message: FLocal.LocalizedString("LOGIN_FAIL", comment: ""), preferredStyle: .Alert)
        }
        
        alert.addAction(UIAlertAction(title: FLocal.LocalizedString("OK", comment: ""), style: .Default, handler: nil))
        alert.presentViewController(alert, animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        alert_Refreshing.dismissViewControllerAnimated(true, completion: nil)
    }
    

}
