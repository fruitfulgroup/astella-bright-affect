//
//  CoursesTableView.swift
//  Bright Affect
//
//  Created by Zee on 21/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData

class CoursesTableView: UITableViewController {
    
    private var requestCourses:JSONUrlRequest = JSONUrlRequest()
    private var requestVideos:JSONUrlRequest = JSONUrlRequest()
    private var courses = [NSManagedObject]()
    private var videos = [NSManagedObject]()
    private var courseImages = [String: UIImage]()
    private var saveOffline = NSMutableDictionary()

    private var appDelegate:AppDelegate!
    private var managedContext:NSManagedObjectContext!
    
    var downloadingAlert: UIAlertController!
    var downloading:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imgView = UIImageView(image: UIImage(named: "astella long"))
        imgView.contentMode = .ScaleAspectFit
        imgView.frame = CGRect(x: imgView.frame.origin.x, y: imgView.frame.origin.y, width: imgView.frame.width, height: 20)
        
        self.navigationItem.titleView = imgView
        
        self.navigationController?.navigationBar.topItem?.title = " "
        
        let eButton = UIBarButtonItem(title: FLocal.LocalizedString("SAVE_OFFLINE", comment: ""), style: .Plain, target: self, action: "editButtonPressed:")
        self.navigationItem.rightBarButtonItem = eButton
        self.navigationItem.leftBarButtonItem?.title = " "
        self.navigationController?.navigationBar.topItem?.title = " "
        
        self.refreshControl?.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        
        
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedContext = self.appDelegate.managedObjectContext
        
        
        if let cs = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Courses"))) as? [NSManagedObject] {
            courses = cs
        }
        
        if let vd = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Videos"))) as? [NSManagedObject] {
            videos = vd
        }
        
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let y = standardUserDefaults.objectForKey("offline") as? NSDictionary {
            saveOffline = NSMutableDictionary(dictionary: y)
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        downloading = false
        ReloadTable()
        
        
        self.DownloadPhotos()
        
        downloadingAlert = UIAlertController(title: FLocal.LocalizedString("DOWNLOADING", comment: ""), message: nil, preferredStyle: .Alert)
        
        
        if courses.count <= 0 {
            self.Refresh(self.navigationItem.rightBarButtonItem!)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let y = self.tableView.indexPathsForSelectedRows {
            self.tableView.beginUpdates()
            self.tableView.reloadRowsAtIndexPaths(y, withRowAnimation: UITableViewRowAnimation.Fade)
            self.tableView.endUpdates()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ReloadTable(){
        self.downloading = false
        self.tableView.reloadData()
    }
    
    
    func UpdateSuccess(text:String) {
        requestVideos = JSONUrlRequest()
        requestVideos.SetURL(UrlString: "\(Fruitful.API_URL)GetVideos")
        // requestVideos.AddParameter(Name: "Username", Value: FLoginControl.UserName())
        //requestVideos.AddParameter(Name: "Token", Value: FLoginControl.Token())
        requestVideos.AddParameter(Name: "ClientID", Value: FLoginControl.ClientID())
        requestVideos.AddParameter(Name: "LanguageID", Value: "\(Fruitful.LANGUAGE_ID)")
        
        
        requestVideos.OnRequestSuccess = UpdateCourseVideoData
        requestVideos.OnRequestFail = UpdateFail
        
        requestVideos.GetJsonFromUrlRequest(ShowAlert: false)
    }
    
    
    func UpdateCourseVideoData(text:String) {
        
        downloading = true
        
        dispatch_async(dispatch_get_main_queue(), {
            self.downloadingAlert.dismissViewControllerAnimated(false, completion: nil)
        })
        
        courses.removeAll(keepCapacity: false);
        videos.removeAll(keepCapacity: false);
        
        //Clear/Delete old video data first
        if let oldCourses = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Courses"))) as? [NSManagedObject] {
            for course:NSManagedObject in oldCourses {
                managedContext.deleteObject(course)
            }
        }
        
        
        if let oldVids = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Videos"))) as? [NSManagedObject] {
            for vid:NSManagedObject in oldVids {
                managedContext.deleteObject(vid)
            }
        }
        
        do {
            try managedContext.save()
        } catch _ {
        }
        
        
        //Add new course data
        for dic in requestCourses.ParsedObjects{
            _ = dic as? NSDictionary
            
            let course = NSManagedObject(entity: NSEntityDescription.entityForName("Courses", inManagedObjectContext: managedContext)!, insertIntoManagedObjectContext:managedContext)
            
            if let t:Int = dic.objectForKey("ClientID") as? Int { course.setValue(t, forKey: "client_id" ) }
            if let t:Int = dic.objectForKey("CourseID") as? Int { course.setValue(t, forKey: "course_id" ) }
            if let t:Int = dic.objectForKey("LanguageID") as? Int { course.setValue(t, forKey: "language_id" ) }
            if let t:String = dic.objectForKey("Title") as? String { course.setValue(t, forKey: "title" ) }
            if let t:String = dic.objectForKey("Overview") as? String { course.setValue(t, forKey: "overview" ) }
            if let t:String = dic.objectForKey("Subject") as? String { course.setValue(t, forKey: "subject" ) }
            if let t:String = dic.objectForKey("PhotoLink") as? String {
                course.setValue(t.stringByReplacingOccurrencesOfString("~/", withString: Fruitful.URL, options: NSStringCompareOptions.LiteralSearch, range: nil), forKey: "photo_link" )
                
                let fullArr = t.characters.split {$0 == "/"}.map { String($0) }
                if let p = fullArr.last { course.setValue(p, forKey: "photo" ) }
            }
            
            do {
                try managedContext.save()
            } catch _ {
            }
            courses.append(course)
        }
        //Add new video data
        for dic in requestVideos.ParsedObjects{
            _ = dic as? NSDictionary
            let vid = NSManagedObject(entity: NSEntityDescription.entityForName("Videos", inManagedObjectContext: managedContext)!, insertIntoManagedObjectContext:managedContext)
            
            if let t:Int = dic.objectForKey("ClientID") as? Int { vid.setValue(t, forKey: "client_id" ) }
            if let t:Int = dic.objectForKey("CourseID") as? Int { vid.setValue(t, forKey: "course_id" ) }
            if let t:Int = dic.objectForKey("LanguageID") as? Int { vid.setValue(t, forKey: "language_id" ) }
            if let t:Int = dic.objectForKey("Position") as? Int { vid.setValue(t, forKey: "position" ) }
            if let t:String = dic.objectForKey("Script") as? String { vid.setValue(t, forKey: "script" ) }
            if let t:String = dic.objectForKey("Title") as? String { vid.setValue(t, forKey: "title" ) }
            if let t:String = dic.objectForKey("LastUpdated") as? String { vid.setValue(Fruitful.DateFromJSON(t) , forKey: "last_updated" )  }
            
            if let t:String = dic.objectForKey("VideoLink") as? String {
                vid.setValue(t, forKey: "video_link" )
                
                let fullArr = t.characters.split {$0 == "/"}.map { String($0) }
                if let v = fullArr.last { vid.setValue(v, forKey: "video" ) }
            }
            
            do {
                try managedContext.save()
            } catch _ {
            }
            videos.append(vid)
        }
        
        downloading = false
        //Dlelete videos that are not listed here in folder.
        self.performSelectorOnMainThread("ReloadTable", withObject: nil, waitUntilDone: true)
        
        
        DownloadPhotos()
        
    }
    
    
    
    func UpdateFail(text:String){
        
        self.downloadingAlert.dismissViewControllerAnimated(false, completion: nil)
        
        let alert = UIAlertController(title: nil, message: FLocal.LocalizedString("DATABASE_CONNECT_FAIL", comment: ""), preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: FLocal.LocalizedString("OK", comment: ""), style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    
    func DownloadPhotos() {
        
        let priority = DISPATCH_QUEUE_PRIORITY_LOW
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            //let existingPhotos:Array<String> = Fruitful.ListOfFiles(Path: "/Photos/")
            self.downloading = true
            
            for var xx = 0; xx<self.courses.count; ++xx {
                
                let cs = self.courses[xx]
                
                if let file:String = cs.valueForKey("photo") as? String, link:String = cs.valueForKey("photo_link") as? String {
                    
                    if !Fruitful.FileExists(Filename: file, Path: "/Photos/") {
                        
                        if let link_encoded = link.stringByReplacingOccurrencesOfString("~/", withString: Fruitful.API_URL).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()){
                            
                            if let url = NSURL(string: link_encoded) {
                                if let data:NSData = NSData(contentsOfURL: url){
                                    if let img = UIImage(data: data) {
                                        
                                        Fruitful.SaveFile(data, Filename: file, Path: "/Photos/")
                                        self.courseImages[file] = img
                                    }
                                }
                            }
                        }
                        
                    }
                    else {
                        let data = Fruitful.LoadFile(Filename: file, Path: "/Photos/")
                        if let img = UIImage(data: data) {
                            self.courseImages[file] = img
                        }
                    }
                    
                }
            }
            
            self.downloading = false
            self.performSelectorOnMainThread("ReloadTable", withObject: nil, waitUntilDone: true)
            
        }
        
    }
    
    func DownloadVideos() {
        
        let priority = DISPATCH_QUEUE_PRIORITY_LOW
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            self.downloading = true
            
            dispatch_async(dispatch_get_main_queue(), {
                self.presentViewController(self.downloadingAlert, animated: false, completion: nil)
            })
            
            
            //Delete videos not in list
            let listAllVideos = Fruitful.ListOfFiles(Path: "/Videos/")
            var found = false
            var videoName = ""
            
            for file in listAllVideos {
                found = false
                
                for vid in self.videos {
                    videoName = vid.valueForKey("video") as! String
                    if videoName == file { found = true;  break; }
                }
                
                if !found {
                    Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                }
                
            }
            
            for vid in self.videos {
                
                if let videoName = vid.valueForKey("video") as? String {
                    
                    if self.saveOffline.valueForKey("course_id")?.boolValue == false {
                        Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                        continue
                    }
                }
                
            }
            
            var numVideos = 0
            
            for _ in self.videos {
                
                if let save  = self.saveOffline.valueForKey("course_id") as? Bool {
                    if save == true {
                        numVideos++
                    }
                    else {
                        Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                    }
                }
                else {
                    Fruitful.DeleteFile(Filename: videoName, Path: "/Videos/")
                }
            }
            
            var current = 1;
            for vid in self.videos {
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.downloadingAlert.title = FLocal.LocalizedString("DOWNLOADING", comment: "")  + " \(current) / \(self.videos.count)"
                    current++
                })
                
                
                let videoName = vid.valueForKey("video") as! String
                let videolink = vid.valueForKey("video_link") as! String
                
                
                if let save  = self.saveOffline.valueForKey("course_id") as? Bool {
                    if save == false {
                        continue
                    }
                }
                else {
                    continue
                }
                
                let updated = vid.valueForKey("last_updated") as! NSDate
                
                // Date comparision to compare current date and end date.
                let dateComparisionResult:NSComparisonResult = updated.compare(Fruitful.FileCreatedDate(Filename: videoName, Path: "/Videos"))
                
                
                
                if !Fruitful.FileExists(Filename: videoName, Path: "/Videos/") || dateComparisionResult == NSComparisonResult.OrderedDescending {
                    
                    if let link_encoded = videolink.stringByReplacingOccurrencesOfString("~/", withString: Fruitful.API_URL).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()){
                        
                        if let url = NSURL(string: link_encoded ) {
                            if let data:NSData = NSData(contentsOfURL: url){
                                
                                Fruitful.SaveFile(data, Filename: videoName, Path: "/Videos/")
                                print("downloaded")
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.downloadingAlert.title = "\(current/numVideos*100)%"
                                })
                                
                            }
                        }
                    }
                    
                }
                
            }
            
            self.downloading = false
            
            dispatch_async(dispatch_get_main_queue(), {
                sleep(1)
                self.downloadingAlert.title = FLocal.LocalizedString("DOWNLOADING", comment: "")
                //self.downloadingAlert.dismissWithClickedButtonIndex(0, animated: false)
                self.downloadingAlert.dismissViewControllerAnimated(false, completion: nil)
                self.performSelectorOnMainThread("ReloadTable", withObject: nil, waitUntilDone: true)
                let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                standardUserDefaults.setObject(self.saveOffline, forKey: "offline")
                NSUserDefaults.standardUserDefaults().synchronize()
                
            })
            
        }
    }
    
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        
        Refresh(self.navigationItem.rightBarButtonItem!)
        refreshControl.endRefreshing()
    }
    
    @IBAction func Refresh(sender: UIBarButtonItem) {
        
        if !self.tableView.editing {
            
            requestCourses = JSONUrlRequest()
            requestCourses.SetURL(UrlString: "\(Fruitful.API_URL)GetCourses")
            requestCourses.AddParameter(Name: "Username", Value: FLoginControl.UserName())
            requestCourses.AddParameter(Name: "Token", Value: FLoginControl.Token())
            requestCourses.AddParameter(Name: "ClientID", Value: FLoginControl.ClientID())
            requestCourses.AddParameter(Name: "LanguageID", Value: "\(Fruitful.LANGUAGE_ID)")
            
            requestCourses.OnRequestSuccess = UpdateSuccess
            requestCourses.OnRequestFail = UpdateFail
            
            self.presentViewController(self.downloadingAlert, animated: false, completion: nil)
            requestCourses.GetJsonFromUrlRequest(ShowAlert: false)
        }
    }
    
    @IBAction func BackToLanguageSelect(sender: UIButton) {
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        standardUserDefaults.removeObjectForKey("LangID")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
        
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return courses.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CourseCell", forIndexPath: indexPath) as! CourseTableCell
        
        if downloading == true { return cell }
        
        if courses.count < indexPath.row {
            return cell
        }
        
        let cs = courses[indexPath.row]
        
        if let y = cs.valueForKey("title") as? String { cell.l_title.text = y }
        if let y = cs.valueForKey("subject") as? String { cell.l_subtitle.text = y }
        if let y = cs.valueForKey("overview") as? String { cell.tv_overview.text = y }
        
        if let y = cs.valueForKey("photo") as? String {
            
            if let img = courseImages[y] {
                cell.iv_photo.image = img
            }
            
        }
        
        let courseID = (cs.valueForKey("course_id") as? Int)!
        
        if let download:Bool = self.saveOffline.valueForKey("\(courseID)") as? Bool {
            cell.l_offline.hidden = download == true ? false : true
        }
        else{
            cell.l_offline.hidden = true
        }
        
        
        return cell
    }
    
    @IBAction func editButtonPressed(editButton: UIBarButtonItem) {
        
        if (self.tableView.editing) {
            
            for cs in courses{
                let courseID:Int = cs.valueForKey("course_id") as! Int
                self.saveOffline.setValue(false, forKey: "\(courseID)")
            }
            
            if let indexes = self.tableView.indexPathsForSelectedRows {
                
                if indexes.count > 0 {
                    
                    for indx in indexes{
                        let courseID:Int = self.courses[indx.row].valueForKey("course_id") as! Int
                        self.saveOffline.setValue(true, forKey: "\(courseID)")
                    }
                }
                
                DownloadVideos();
            }
            
            
            
            editButton.title = FLocal.LocalizedString("SAVE_OFFLINE", comment: "")
            self.tableView.setEditing(false, animated: true)
            
        }
        else {
            editButton.title = FLocal.LocalizedString("DONE", comment: "")
            self.tableView.setEditing(true, animated: true)
            
            for var index = 0; index < self.courses.count; ++index {
                let cs = self.courses[index]
                let courseID:Int = cs.valueForKey("course_id") as! Int
                
                if self.saveOffline.valueForKey("\(courseID)")?.boolValue == true {
                    self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
                }
            }
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return (self.tableView.editing) ? false : true
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if let vc:PlayCourseVideo = segue.destinationViewController as? PlayCourseVideo  {
            
            if segue.identifier == "GoToCourse" {
                let index = self.tableView.indexPathForSelectedRow!.row
                
                vc.course = courses[index]
                let courseId = vc.course.valueForKey("course_id") as! Int
                
                if let y = vc.course.valueForKey("photo") as? String {
                    if let p = courseImages[ y] {
                        vc.photo = p
                    }
                }
                
                vc.saveOffline = self.saveOffline
                
                var courseVideos = Array<NSManagedObject>()
                
                for vid in videos {
                    
                    if vid.valueForKey("course_id") as? Int == courseId{
                        courseVideos.append(vid)
                    }
                    
                }
                
                vc.videos = courseVideos
                
            }
            
        }
    }
    
    
}
