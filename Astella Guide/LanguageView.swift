//
//  LanguageView.swift
//  Bright Affect
//
//  Created by Zee on 20/05/2015.
//  Copyright (c) 2015 Bright Affect. All rights reserved.
//

import UIKit
import CoreData

class LanguageView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var languages = [NSManagedObject]()
    
    private var requestLanguages:JSONUrlRequest = JSONUrlRequest()
    private var appDelegate:AppDelegate!
    private var managedContext:NSManagedObjectContext!
    
    @IBOutlet var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup Languages
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.managedContext = self.appDelegate.managedObjectContext
        
        
        if let lg = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Languages"))) as? [NSManagedObject] {
            languages = lg
        }
        
        requestLanguages = JSONUrlRequest()
        requestLanguages.SetURL(UrlString: "\(Fruitful.API_URL)GetLanguages")
        requestLanguages.AddParameter(Name: "ClientID", Value: FLoginControl.ClientID())
        
        requestLanguages.OnRequestSuccess = UpdateData
        requestLanguages.OnRequestFail = UpdateFail
        
        requestLanguages.GetJsonFromUrlRequest(ShowAlert: false)
        
        
        
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
               
        let y = standardUserDefaults.objectForKey("LangID") as? Int
        
        if y != nil {
            GoToLogin(y!, animated: false)
            return
        }
        
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.flashScrollIndicators()
        
        
    }
    
    
    func UpdateData(text:String) {
        
        languages.removeAll(keepCapacity: false);
        
        //Clear/Delete old video data first
        if let oldLang = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Languages"))) as? [NSManagedObject] {
            for lang:NSManagedObject in oldLang {
                managedContext.deleteObject(lang)
            }
        }
        
        do {
            try managedContext.save()
        }
        catch _ { }
        
        
        //Add new course data
        for dic in requestLanguages.ParsedObjects{
            
            let lang = NSManagedObject(entity: NSEntityDescription.entityForName("Languages", inManagedObjectContext: managedContext)!, insertIntoManagedObjectContext:managedContext)
            
            if let t:Int = dic.objectForKey("ID") as? Int { lang.setValue(t, forKey: "id" ) }
            if let t:String = dic.objectForKey("Language") as? String { lang.setValue(t, forKey: "language" ) }
            if let t:String = dic.objectForKey("Translation") as? String { lang.setValue(t, forKey: "translation" ) }
                        
            do {
                try managedContext.save()
            } catch _ {
            }
            languages.append(lang)
        }
        
        self.performSelectorOnMainThread("ReloadTable", withObject: nil, waitUntilDone: true)
        
    }
    
    
    
    func UpdateFail(text:String){
       
        let alert = UIAlertController(title: nil, message: FLocal.LocalizedString("DATABASE_CONNECT_FAIL", comment: ""), preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: FLocal.LocalizedString("OK", comment: ""), style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    func ReloadTable() {        
        self.tableView.reloadData()
        self.tableView.flashScrollIndicators()
    }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func LanguageSelected(sender: UIButton) {
        
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let y = standardUserDefaults.objectForKey("PreLangID")?.integerValue {
            
            if y != sender.tag {
                Fruitful.DeleteAllFiles(Path: "/Videos/")
                Fruitful.DeleteAllFiles(Path: "/Photos/")
                
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                if let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext {
                
                    //Clear/Delete old video data first

                    if let oldCourses = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Courses"))) as? [NSManagedObject] {
                        for course:NSManagedObject in oldCourses {
                            managedContext.deleteObject(course)
                        }
                    }
                
                    if let oldVids = (try? managedContext.executeFetchRequest(NSFetchRequest(entityName: "Videos"))) as? [NSManagedObject] {
                        for vid:NSManagedObject in oldVids {
                            managedContext.deleteObject(vid)
                        }
                    }
                
                    do {
                        try managedContext.save()
                    } catch _ {
                    }
                    
                }

            }
        }
        
        GoToLogin(sender.tag, animated: true)

    }
    
    private func GoToLogin(LangID: Int, animated: Bool) {
        let standardUserDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        Fruitful.LANGUAGE_ID = LangID
        
        switch(LangID) {
            
            case 5: FLocal.InitLocalWithCode(Code: "bg"); break
            case 13: FLocal.InitLocalWithCode(Code: "cs"); break
            case 9: FLocal.InitLocalWithCode(Code: "de"); break
            case 15: FLocal.InitLocalWithCode(Code: "nl"); break
            case 6: FLocal.InitLocalWithCode(Code: "el"); break
            case 1: FLocal.InitLocalWithCode(Code: "en"); break
            case 3: FLocal.InitLocalWithCode(Code: "es"); break
            case 2: FLocal.InitLocalWithCode(Code: "fr"); break
            case 17: FLocal.InitLocalWithCode(Code: "fr-BE"); break
            case 12: FLocal.InitLocalWithCode(Code: "hu"); break
            case 8: FLocal.InitLocalWithCode(Code: "it"); break
            case 4: FLocal.InitLocalWithCode(Code: "ja"); break
            case 7: FLocal.InitLocalWithCode(Code: "ko"); break
            case 14: FLocal.InitLocalWithCode(Code: "pl"); break
            case 11: FLocal.InitLocalWithCode(Code: "pt"); break
            case 16: FLocal.InitLocalWithCode(Code: "sk"); break
            case 10: FLocal.InitLocalWithCode(Code: "tr"); break
            default: FLocal.InitLocalWithCode(Code: ""); break
        }
        
        
        standardUserDefaults.setObject(LangID, forKey: "LangID");
        standardUserDefaults.setObject(LangID, forKey: "PreLangID");
        NSUserDefaults.standardUserDefaults().synchronize()
        
        
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("CoursesTableView") as? CoursesTableView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        
        /*
        if let view = self.storyboard?.instantiateViewControllerWithIdentifier("LoginView") as? LoginView {
            self.navigationController?.pushViewController(view, animated: animated)
        }
        */

    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return languages.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LanguageCell", forIndexPath: indexPath) as! LanguageCell
        
        if languages.count >= indexPath.row {
            let vid = languages[indexPath.row]
        
            if let y = vid.valueForKey("translation") as? String {
                cell.b_language.setTitle(y, forState: UIControlState.Normal)
            }
        
            if let y = vid.valueForKey("id") as? Int {
                cell.b_language.tag = y
            }
        }
        
        return cell
    }
        
}
